sardana (3.5.1-2) unstable; urgency=medium

  * Bug fix: "FTBFS: unsatisfiable build-dependencies: python3-pylsp (&lt;
    1.11~), python3-qtconsole (&lt; 5.6~), python3-spyder-kernels (&lt;
    2.6~)", thanks to Lucas Nussbaum (Closes: #1091057).

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 05 Mar 2025 15:12:53 +0100

sardana (3.5.1-1) unstable; urgency=medium

  * d/watch: updated to use plain searchmode for gitlab
  * New upstream version 3.5.1
  Use secure URI in Homepage field.
  Update standards version to 4.6.2, no changes needed.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 05 Mar 2025 15:02:28 +0100

sardana (3.5.0-2) unstable; urgency=medium

  * Team Upload.
  * d/patches/0002-Fix-SyntaxWarning.patch: Add patch to remove
    SyntaxWarnings (Closes: #985008).

 -- Emmanuel Arias <eamanu@debian.org>  Mon, 11 Nov 2024 16:42:00 -0300

sardana (3.5.0-1) unstable; urgency=medium

  * Add myself to Uploaders.
  * Update debian/watch to follow upstream releases.
  * New upstream release.

 -- Roland Mas <lolando@debian.org>  Tue, 16 Jul 2024 15:39:06 +0200

sardana (3.0.3-1) unstable; urgency=medium

  * New upstream version 3.0.3
  * Standards-Version bump to 4.5.0 (nothing to do)
  * debhelper-compat bump to 13 (nothing to do)

 -- Carlos Pascual <cpascual@cells.es>  Wed, 07 Oct 2020 12:30:00 +0000

sardana (3.0.2a0+34.9f6895-1) unstable; urgency=medium

  * New upstream version 3.0.2a0+34.9f6895
  * Fixed reprotest issues in pyhton3-sardana-doc
  * Removed unneeded patch
  * python3-sardana breaks+replaces python-sardana (closes: #943847)

 -- Carlos Pascual <cpascual@cells.es>  Thu, 14 Nov 2019 13:36:28 +0000

sardana (3.0.0a+3.f4f89e+dfsg-2) unstable; urgency=medium

  * Source only upload

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 19 Oct 2019 13:30:38 +0200

sardana (3.0.0a+3.f4f89e+dfsg-1) unstable; urgency=medium

  [ Carlos Pascual ]
  * New upstream version 3.0.0a+3.f4f89e
  * Python 2 removal: binary package changed to python3-sardana
  * Standards-Version bump to 4.4.0 (nothing to do)
  * Use debhelper-compat instead of debian/compat.
  * d/changelog: Remove trailing whitespaces.
  * Override version info in python3-tango dep

 -- Carlos Pascual <cpascual@cells.es>  Tue, 03 Sep 2019 17:55:00 +0100

sardana (2.6.2+dfsg-1) unstable; urgency=medium

  * New upstream version 2.6.2

 -- Carlos Pascual <cpascual@cells.es>  Tue, 05 Feb 2019 11:19:28 +0100

sardana (2.6.1+dfsg-1) unstable; urgency=medium

  * New upstream version 2.6.1 (closes: #918830 )
  * Standards-Version bump to 4.3.0 (nothing to do)
  * Update upstream info in d/copyright
  * Make debian/watch file a dummy one
  * Drop existing quilt patch queue
  * Add patch to avoid privacy-breach lintian error
  * Fix various minor lintian errors
  * Enable Salsa-Pipeline for building and testing packages

 -- Carlos Pascual <cpascual@cells.es>  Mon, 04 Feb 2019 15:47:31 +0100

sardana (2.2.2-3) unstable; urgency=medium

  * Avoid dependency on nxs python module
  * Remove python-nxs depend and add Uploader

 -- Carlos Pascual <cpascual@cells.es>  Fri, 12 May 2017 01:38:55 +0200

sardana (2.2.2-2) unstable; urgency=medium

  * Unit test unactivated for now (Closes: #851017)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 13 Jan 2017 14:08:16 +0100

sardana (2.2.2-1) unstable; urgency=medium

  * New upstream version 2.2.2

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 11 Jan 2017 17:59:25 +0100

sardana (2.2.0-1) unstable; urgency=medium

  * New upstream version 2.2.0
  * Standards-Version bump to 3.9.8 (nothing to do)
  * Switched to pybuild build system
  * compat level 9 -> 10
  * d/control
    - Added Build-Depends: python-setuptools
    - Added Suggests: python-sardana-doc (Closes: #849615)
      Thanks Ben Finney for the bug report

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 31 Dec 2016 10:00:54 +0100

sardana (2.1.1-1) unstable; urgency=medium

  * Uploaded into Unstable

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 31 Oct 2016 20:41:26 +0100

sardana (2.1.1-1~exp1) experimental; urgency=medium

  * New upstream version 2.1.1

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 30 Sep 2016 10:59:28 +0200

sardana (2.1.0-1~exp1) experimental; urgency=medium

  * New upstream version 2.1.0

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 13 Sep 2016 20:41:26 +0200

sardana (2.0.0-1) unstable; urgency=medium

  * Imported Upstream version 2.0.0
  * Fixed VCS-x urls with cme
  * debian/control
    - Depends: gir1.2-hkl-5.0

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 29 Apr 2016 00:58:49 +0200

sardana (1.6.1-1) unstable; urgency=medium

  * Imported Upstream version 1.6.1
  * debian/control
    - Depends: taurus (>= 3.6.0+dfsg-1~)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 28 Jul 2015 15:16:42 +0200

sardana (1.5.0-1~exp1) experimental; urgency=medium

  * Imported Upstream version 1.5.0
  * Standards-Version bump to 3.9.6 (nothing to do)
  * debian/watch use the pypi redirector.
  * debian/control
    + Depends: taurus >= 3.4.0+dfsg-1~
    + Build-Depends: texlive-latex-base, texlive-latex-extra, dvipng
      for the documentation
    + Build-Depends: dh-python which was missing

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 06 Feb 2015 18:38:07 +0100

sardana (1.4.2-1) unstable; urgency=high

  * Imported Upstream version 1.4.2

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 27 Oct 2014 12:11:35 +0100

sardana (1.4.0-1) unstable; urgency=low

  * Imported Upstream version 1.4.0
  * debian/control
    - Add Breaks, Replaces for python-taurus (<< 3.3.0+dfsg-2~)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 06 Aug 2014 09:49:59 +0200

sardana (1.3.1-2) unstable; urgency=medium

  * debian/control
    - python-sardana: Depends python-nxs | libnexus0-python
      to prepare the backport.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 13 Apr 2014 12:08:37 +0200

sardana (1.3.1-1) unstable; urgency=medium

  * Imported Upstream version 1.3.1
  * debian/patches
    - 0001-forwarded-upstream-install-also-the-ipython_01_00-mo.patch (removed)
  * debian/control
    - Depends: ipython-qtconsole for spock

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 17 Feb 2014 13:52:26 +0100

sardana (1.3.0-1) unstable; urgency=medium

  * Imported Upstream version 1.3.0
  * Bump Standard-Versions to 3.9.5 (Nothing to do)
  * debian/control
    - Improve the sardana package description.
      Thanks to Justin B Rye for this (Closes: #735164)
  * debian/patches
    - 0001-fix-the-FTBFS-until-the-PyTango-binding-is-fixed.patch
      the real problem was sphinx 1.2.0, now that 1.2.1 is available
      remove the patch.
    + 0001-forwarded-upstream-install-also-the-ipython_01_00-mo.patch
      the default upstream forgot to install the ipython_01_00 module

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 16 Feb 2014 07:21:33 +0100

sardana (1.2.0-2) unstable; urgency=low

  * debian/control
    - Add Depends:python-nxs to allow NeXuS file generation
  * debian/rules
    - export http_proxy='localhost' to avoid internet download during
      the build (Closes: #735161)
    - remove duplication in the documentation package
  * debian/patch
    + 0001-fix-the-FTBFS-until-the-PyTango-binding-is-fixed.patch
      remove the inheritance-diagram from the documentation

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 09 Sep 2013 11:26:31 +0200

sardana (1.2.0-1) unstable; urgency=low

  * Initial release (Closes: #717871)
  * Thanks to Justin B Rye for package description review

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 26 Jul 2013 18:53:05 +0200
